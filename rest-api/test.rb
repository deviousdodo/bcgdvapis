require 'minitest/autorun'
require 'minitest/reporters'
require 'rack/test'

require './app'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

def esc(str)
  CGI::escape(str)
end

describe 'API' do
  include Rack::Test::Methods

  def app
    PublicTransport::API.transport_data = PublicTransport::TransportData.new('./test-data')
    PublicTransport::API
  end

  describe '/' do

    it 'shows greeting' do
      get '/'
      assert last_response.ok?
      assert JSON.parse(last_response.body).has_key?('greeting')
    end

  end

  describe '/swagger' do

    it 'provides swagger json' do
      get '/swagger'
      assert last_response.ok?
      assert JSON.parse(last_response.body).has_key?('swagger')
    end

  end


  describe '/lines' do

    it 'requires a timestamp' do
      get '/lines?x=10&y=12'
      refute last_response.ok?
    end

    it 'requires an x coordinate' do
      get "/lines?timestamp=#{esc('10:00:00')}&y=12"
      refute last_response.ok?
    end

    it 'requires an y coordinate' do
      get "/lines?timestamp=#{esc('10:00:00')}&x=10"
      refute last_response.ok?
    end

    it 'returns an array of matching line names' do
      get "/lines?timestamp=#{esc('10:23:00')}&x=4&y=9"
      assert last_response.ok?
      assert_equal({ "lines" => ['S75'] }, JSON.parse(last_response.body))
    end

    it 'takes into account delays' do
      get "/lines?timestamp=#{esc('10:13:00')}&x=4&y=9"
      assert last_response.ok?
      assert_equal({ "lines" => [] }, JSON.parse(last_response.body))
    end

    it 'returns an empty array when no line matches' do
      get "/lines?timestamp=#{esc('10:00:00')}&x=10&y=13"
      assert last_response.ok?
    end

  end

  describe '/line/<name>' do

    it 'requires a name' do
      get '/line'
      refute last_response.successful?
    end

    it 'is error if line does not exist' do
      get '/line/does-not-exist'
      assert last_response.not_found?
    end

    it 'is case sensitive with line names' do
      get '/line/m4'
      assert last_response.not_found?
    end

    it 'returns the delay in minutes' do
      get '/line/M4'
      assert_equal({ "delay" => 1 }, JSON.parse(last_response.body))
    end

    it 'returns 0 if line does not have a delay' do
      get '/line/M5'
      assert_equal({ "delay" => 0 }, JSON.parse(last_response.body))
    end

  end

end
