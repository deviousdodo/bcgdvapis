require 'grape'
require 'grape-swagger'
require 'securerandom'

require './transport_data'

module PublicTransport
  class API < Grape::API

    class << self
      attr_accessor :transport_data
    end

    helpers do
      def transport_data
        API.transport_data
      end
    end

    version 'v1', using: :accept_version_header
    format :json

    # The errors would be logged somewhere and could be found using the trace_ids.
    rescue_from Exceptions::NotFound do |e|
      error!({ error: e.message, trace_id: SecureRandom.uuid }, 404)
    end

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!({ error: e.message, trace_id: SecureRandom.uuid }, 400)
    end

    rescue_from :all do |e|
      error!({ error: "Server error!", trace_id: SecureRandom.uuid }, 500)
    end

    desc "Welcoming root URL."
    get '/' do
      { greeting: "Welcome to Verspaetung's public transport API" }
    end

    desc "Find lines available at a given time and station. Takes into account delays."
    params do
      requires :timestamp, type: String, desc: 'Example value 10:00:00.'
      requires :x, type: Integer, desc: 'Example value 10.'
      requires :y, type: Integer, desc: 'Example value 12.'
    end
    get :lines do
      { lines: transport_data.find_lines(params[:timestamp], params[:x].to_s, params[:y].to_s) }
    end

    desc "Retrieve delay of line."
    get "line/:name" do
      { delay: transport_data.line_delay(params[:name]) }
    end

    add_swagger_documentation api_version: 'v1', mount_path: '/swagger', info: {
      title: 'Verspaetung PublicTransport API',
      description: 'Documentation for API v1',
    }

  end
end
