require 'date'
require 'csv'

require './exceptions'

module PublicTransport
  # Loads CSV files into convenient structures and exposes methods to access the data.
  class TransportData

    def initialize path
      @path = path
      load_data!
    end

    def find_lines(timestamp, x, y)
      stop_times = @delayed_stop_times[timestamp] || {}
      stop_times[[x, y]] || []
    end

    def line_delay(name)
      raise Exceptions::NotFound, "Line does not exist!" if !@lines_by_name[name]
      @lines_by_name[name][:delay]
    end

    private

    def add_delay_to_timeline(time, delay)
      now = Time.now
      time_parts = time.split(':').map(&:to_i)
      delayed_time = Time.new(now.year, now.month, now.day, time_parts[0], time_parts[1], time_parts[2]) + delay * 60
      delayed_time.strftime('%H:%M:%S')
    end

    def load_data!
      load_lines
      load_delays
      load_stops
      load_stop_times
    end

    def load_lines
      @lines_by_name = {}
      @lines_by_id = {}
      CSV.foreach("#{@path}/lines.csv", headers: true, header_converters: :symbol) do |row|
        line = { id: row[:line_id], delay: 0, name: row[:line_name] }
        @lines_by_id[row[:line_id]] = @lines_by_name[row[:line_name]] = line
      end
    end

    def load_delays
      CSV.foreach("#{@path}/delays.csv", headers: true, header_converters: :symbol) do |row|
        # small sanity check that we don't have a delay for a non-existing line
        if @lines_by_name[row[:line_name]]
          @lines_by_name[row[:line_name]][:delay] = row[:delay].to_i
        end
      end
    end

    def load_stops
      @stops_by_id = {}
      CSV.foreach("#{@path}/stops.csv", headers: true, header_converters: :symbol) do |row|
        @stops_by_id[row[:stop_id]] = { x: row[:x], y: row[:y] }
      end
    end

    def load_stop_times
      @delayed_stop_times = {}
      CSV.foreach("#{@path}/stop_times.csv", headers: true, header_converters: :symbol) do |row|
        line = @lines_by_id[row[:line_id]]
        stop = @stops_by_id[row[:stop_id]]
        delayed_time = add_delay_to_timeline(row[:time] , line[:delay])

        @delayed_stop_times[delayed_time] ||= {}
        @delayed_stop_times[delayed_time][[stop[:x], stop[:y]]] ||= []
        @delayed_stop_times[delayed_time][[stop[:x], stop[:y]]] << line[:name]
      end
    end

  end
end
