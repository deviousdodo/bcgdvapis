# Verspaetung Public Transport REST API

Available at: https://bcgdv-rest-api.herokuapp.com/

All calls:

- https://bcgdv-rest-api.herokuapp.com/line/S75
- https://bcgdv-rest-api.herokuapp.com/lines?timestamp=10:23:00&x=4&y=9
- https://bcgdv-rest-api.herokuapp.com/swagger

## Run

```
bundle
# run all tests:
bundle exec rake
# run app on port 8081:
bundle exec rake dev
# Go to http://localhost:8081/
```

## Assumptions

- I use case-sensitive names for lines.
- I made all endpoint parameters required.
