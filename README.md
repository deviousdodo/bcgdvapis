# BCG DV challenge

I created a REST API in Ruby as a solution to the challenge, but since I talked so much about
GraphQL in my first interview I decided to also build a very small GraphQL API in Node, as a
showcase and maybe for further discussion :)

The GraphQL API uses the REST API to serve the data, so it doesn't have any logic, and I tried
to model it similar to the challenge specification.

See each folder for setup instructions and links to Heroku where I've deployed them.
