const { ApolloServer, gql } = require("apollo-server");
const axios = require("axios");

const http = axios.create({
  baseURL: process.env.REST_API_URL || "http://localhost:8081",
  timeout: 30000
});

const typeDefs = gql`
  type Query {
    # these 2 mimic the task rest endpoints
    lines(timestamp: String!, x: Int!, y: Int!): [String]
    line(name: String!): Int!
  }
`;

const resolvers = {
  Query: {
    lines: async (parent, args, context) => {
      const response = await http.get("/lines", { params: args });
      return response.data.lines;
    },
    line: async (parent, args, context) => {
      try {
        const response = await http.get(`/line/${args.name}`);
        return response.data.delay;
      } catch (e) {
        throw e.response.data.error;
      }
    }
  }
};

const server = new ApolloServer({ typeDefs, resolvers });

server.listen({ http: { port: process.env.PORT || 8082 } }).then(({ url }) => {
  console.log(`Server listening at ${url}`);
});
