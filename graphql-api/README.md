# Verspaetung Public Transport GraphQL API

Available at: https://bcgdv-graphql-api.herokuapp.com/

Example query:
```
{
  line(name:"S75")
  lines(timestamp:"10:23:00", x:4, y:9)

}
```

## Run
```
yarn
yarn dev
# Go to http://localhost:8082/
```

## Notes

This API doesn't process the data on its own, but uses the REST API.
